/*
 *   P O I R E A U
 */
class Poireau {
    constructor() {
        this.majElements = [];
        this.niveau = new Niveau(this);
        this.vie = new Vie(this.niveau, this);
        this.force = new Caractere("force", this.niveau, this);
        this.sagesse = new Caractere("sagesse", this.niveau, this);
        this.agilite = new Caractere("agilite", this.niveau, this);
        this.resistance = new Caractere("resistance", this.niveau, this);
        this.science = new Caractere("science", this.niveau, this);
        this.magie = new Caractere("magie", this.niveau, this);
        this.frequence = new Frequence(this.niveau, this);
        this.mp = new MP(this.niveau, this);
        this.tp = new TP(this.niveau, this);
        this.listeArmes = null;
        this.armes = new ItemsEquipes(this.niveau, this, [[1, 2], [100, 3], [200, 4]], "arme");
        this.puces = new ItemsEquipes(this.niveau, this, [ [1, 12], [50, 13], [75, 14], [100, 15], [125, 16], [150, 17], [200, 18], [250, 19], [300, 20]], "puce");
        this.simulArme = new SimulationArmes(this);
        this.simulPuce = new SimulationPuces(this);
        this.save = document.getElementById("leek-save");
    }

    reset_leek(data) {
        this.niveau.initValues(data.level, this.niveau.getCapitalForValue(data.level));
        this.vie.initValues(data.life, this.vie.getCapitalForValue(data.life));
        this.force.initValues(data.strength, this.force.getCapitalForValue(data.strength));
        this.sagesse.initValues(data.wisdom, this.sagesse.getCapitalForValue(data.wisdom));
        this.agilite.initValues(data.agility, this.agilite.getCapitalForValue(data.agility));
        this.resistance.initValues(data.resistance, this.resistance.getCapitalForValue(data.resistance));
        this.science.initValues(data.science, this.science.getCapitalForValue(data.science));
        this.magie.initValues(data.magic, this.magie.getCapitalForValue(data.magic));
        this.frequence.initValues(data.frequency, data.frequency-100);
        this.mp.initValues(data.mp, this.mp.getCapitalForValue(data.mp));
        this.tp.initValues(data.tp, this.tp.getCapitalForValue(data.tp));
        let allocated =
            this.vie.capital +
            this.force.capital +
            this.sagesse.capital +
            this.agilite.capital +
            this.resistance.capital +
            this.science.capital +
            this.magie.capital +
            this.frequence.capital +
            this.mp.capital +
            this.tp.capital;
        this.niveau.unallocatedCapital = this.niveau.capital - allocated;
        let weapons = [];
        for (let c=0; c<data.weapons.length; c++) { weapons.push(data.weapons[c].template); }
        this.armes.selectItems(weapons);
        let chips = [];
        for (let c=0; c<data.chips.length; c++) { chips.push(data.chips[c].template); }
        this.puces.selectItems(chips);
        this.majEverybody();
    }

    addMAJable(element) {
        this.majElements.push(element);
    }

    majEverybody() {
        for (let c=0; c<this.majElements.length; c++) {
            this.majElements[c].maj_html_elements();
        }
        this.save.innerHTML = this.encode();
    }

    initArmes(armes) {
        this.armes.set(armes);
        this.simulArme.set(armes);
    }

    initPuces(puces) {
        this.puces.set(puces);
        this.simulPuce.set(puces);
    }

    
    to52(n) {
        let u = n%52;
        let d = Math.floor(n/52);
        return String.fromCharCode(d+(d<26?97:39)) + String.fromCharCode(u+(u<26?97:39));
    }

    to10(m) {
        let d = m.charCodeAt(0);
        let u = m.charCodeAt(1);
        return 52*(d-(d<91?39:97)) + (u-(u<91?39:97));
    }

    encode() {
        let values = [this.niveau.value,
                      this.vie.capital,
                      this.force.capital,
                      this.sagesse.capital,
                      this.agilite.capital,
                      this.resistance.capital,
                      this.science.capital,
                      this.magie.capital,
                      this.frequence.capital,
                      this.mp.capital,
                      this.tp.capital];
        for (let c=0; c<this.armes.items.length; c++) {
            let val = this.armes.items[c].getItemValue();
            if (val != 0) values.push(val);
        }
        for (let c=0; c<this.puces.items.length; c++) {
            let val = this.puces.items[c].getItemValue();
            if (val != 0) values.push(val);
        }
        let msg = "";
        for (let c=0; c<values.length; c++) {
            msg += this.to52(values[c]);
        }
        return msg;
    }

    decode(msg) {
        if (msg.length%2 == 1 || msg.length < 22 || msg.length.length > 70) return null;
        let values = [];
        while (msg.length > 0) {
            values.push(this.to10(msg));
            msg = msg.substring(2);
        }
        let data = {};
        data.level = values[0];
        if (data.level < 1 || data.level > 301) return null;
        data.life = this.vie.getValueForCapital(values[1], data.level);
        data.strength = this.force.getValueForCapital(values[2]);
        data.wisdom = this.sagesse.getValueForCapital(values[3]);
        data.agility = this.agilite.getValueForCapital(values[4]);
        data.resistance = this.resistance.getValueForCapital(values[5]);
        data.science = this.science.getValueForCapital(values[6]);
        data.magic = this.magie.getValueForCapital(values[7]);
        data.frequency = 100+values[8];
        data.mp = this.mp.getValueForCapital(values[9]);
        data.tp = this.tp.getValueForCapital(values[10]);
        let s = 0;
        for (let c=1; c<11; c++) s += values[c];
        if (s > this.niveau.getCapitalForValue(data.level)) return null;
        let k = 11;
        data.weapons = [];
        let lesarmes = this.armes.items[0].itemList;
        while (k < values.length) {
            let c = 0;
            while (c < lesarmes.length) {
                if (lesarmes[c].id == values[k]) break;
                else c++;
            }
            if (c < lesarmes.length) {
                data.weapons.push({"template": values[k]});
                if (lesarmes[c].level > data.level) return null;
                k++;
            } else break;
        }
        data.chips = [];
        let lespuces = this.puces.items[0].itemList;
        while (k < values.length) {
            let c = 0;
            while (c < lespuces.length) {
                if (lespuces[c].id == values[k]) break;
                else c++;
            }
            if (c < lespuces.length) {
                data.chips.push({"template": values[k]});
                if (lespuces[c].level > data.level) return null;
                k++;
            } else break;
        }
        if (k != values.length) return null;
        return data;
    }
}


/*
 *
 *  A B S T R A C T   C A R A C T E R E
 *
 */
class AbstractCaractere {
    constructor(identifiant, poireau) {
        if (this.constructor == AbstractCaractere) {
            throw new Error("Abstract class AbstractCaractere can't be initialized");
        }
        this.nom = identifiant;
        this.poireau = poireau;
        this.poireau.addMAJable(this);
        this.value = 0;
        this.capital = 0;

        this.pt_label = document.getElementById(this.nom+"-nb");
        this.p1_button = document.getElementById(this.nom+"-plus-un");
        this.m1_button = document.getElementById(this.nom+"-moins-un");
        this.p10_button = document.getElementById(this.nom+"-plus-dix");
        this.m10_button = document.getElementById(this.nom+"-moins-dix");
        this.p100_button = document.getElementById(this.nom+"-plus-cent");
        this.m100_button = document.getElementById(this.nom+"-moins-cent");
        this.capital_label = document.getElementById(this.nom+"-capital");
        this.next_label = document.getElementById(this.nom+"-next");

        let myself = this;
        this.p1_button.addEventListener("click", function() { myself.add(1); });
        this.m1_button.addEventListener("click", function() { myself.add(-1); });
        this.p10_button.addEventListener("click", function() { myself.add(10); });
        this.m10_button.addEventListener("click", function() { myself.add(-10); });
        this.p100_button.addEventListener("click", function() { myself.add(100); });
        this.m100_button.addEventListener("click", function() { myself.add(-100); });
    }

    getMinValue() {
        throw new Error("Method getMinValue must be implemented");
    }

    getMaxValue() {
        throw new Error("Method getMaxValue must be implemented");
    }

    add_value(nb) {
        throw new Error("Method add_value must be implemented");
    }

    get_next_cost() {
        throw new Error("Method get_next_cost must be implemented");
    }
    
    initValues(val, cap) {
        this.value = val;
        this.capital = cap;
    }
    
    add(nb) {
        this.add_value(nb);
        this.poireau.majEverybody();
    }
    
    maj_html_elements() {
        this.maj_labels();
        this.maj_buttons();
    }
    
    maj_labels() {
        this.pt_label.innerHTML = this.value;
        this.capital_label.innerHTML = this.capital;
        if (this.next_label !== null) this.next_label.innerHTML = this.get_next_cost();
    }

    maj_buttons() {
        let mini = this.getMinValue();
        let maxi = this.getMaxValue();
        this.p1_button.disabled = (this.value +1 > maxi);
        this.m1_button.disabled = (this.value -1 < mini);
        this.p10_button.disabled = (this.value +10 > maxi);
        this.m10_button.disabled = (this.value -10 < mini);
        this.p100_button.disabled = (this.value +100 > maxi);
        this.m100_button.disabled = (this.value -100 < mini);
    }
}



/*
 *
 *  N I V E A U
 *
 */
class Niveau extends AbstractCaractere {
    constructor(poireau) {
        super("niveau", poireau);
        this.unallocatedCapital = 50;
        this.unallocatedCapital_label = document.getElementById("capital-nb");
        this.initValues(1, this.unallocatedCapital);
    }

    getMinValue() {
        let investcap = this.capital - this.unallocatedCapital;
        let niv = 1;
        while (this.getCapitalForValue(niv) < investcap) {
            niv += 1;
        }
        let lvl = this.poireau.armes.get_niveau_required();
        niv = (lvl < niv ? niv : lvl);
        lvl = this.poireau.puces.get_niveau_required();
        niv = (lvl < niv ? niv : lvl);
        return niv;
    }

    getMaxValue() {
        return 301;
    }

    add_value(nb) {
        let investcap = this.capital - this.unallocatedCapital;
        this.value += nb;
        this.capital = this.getCapitalForValue(this.value);
        this.unallocatedCapital = this.capital - investcap;
    }

    get_next_cost() {
        // pas implémenté car pas utilisé (pour l'instant..?)
        return 0;
    }
    
    maj_labels() {
        super.maj_labels();
        this.unallocatedCapital_label.innerHTML = this.unallocatedCapital;
    }
    
    getCapitalForValue(niv) {
        return 5*niv + 45*(1 + parseInt(niv/100)) + (niv == 301 ? 95 : 0);
    }

    allocate_capital(nb) {
        this.unallocatedCapital -= nb;
    }
}


/*
 *
 *   F R E Q U E N C E
 *
 */
class Frequence extends AbstractCaractere {
    constructor(niveau, poireau) {
        super("frequence", poireau);
        this.niveau = niveau;
        this.initValues(100,0);
    }
    
    getMinValue() {
        return 100;
    }

    getMaxValue() {
        return this.value + this.niveau.unallocatedCapital;
    }
    
    add_value(nb) {
        this.value += nb;
        this.capital += nb;
        this.niveau.allocate_capital(nb);
    }

    get_next_cost() {
        return 1;
    }
}


/*
 *
 *   V I E
 *
 */
class Vie extends AbstractCaractere {
    constructor(niveau, poireau) {
        super("vie", poireau);
        this.niveau = niveau;
        this.initValues(this.getMinValue(), 0);
    }
    /*
      Vie < 1000            1 Capital pour 4 Points de Vie
      1000 <= Vie < 2000    1 Capital pour 3 Points de Vie
      2000 <= Vie           1 Capital pour 2 Points de Vie
     */
    getMinValue(level) {
        if (level == null)
            level = this.niveau.value;
        return 97 + level*3;
    }

    getMaxValue() {
        return this.getValueForCapital(this.capital + this.niveau.unallocatedCapital);
    }
    
    add_value(nb) {
        let carac = this.value+nb;
        let cap = this.getCapitalForValue(carac);
        while (parseInt(cap) != cap) {
            carac += (nb > 0 ? 1 : -1);
            cap = this.getCapitalForValue(carac);
        }
        this.value = carac;
        this.niveau.allocate_capital(cap - this.capital);
        this.capital = cap;
    }

    get_next_cost() {
        return Math.round(100*(this.getCapitalForValue(this.value+1) - this.capital))/100;
    }
    
    maj_labels() {
        this.value = this.getValueForCapital(this.capital);
        super.maj_labels();
    }
    
    getValueForCapital(cap, level) {
        let viemin = this.getMinValue(level);
        if (cap < 250) return viemin + 4*cap;
        if (cap < 584) return viemin + 3*cap + 250;
        return viemin + 2*cap + 834;
    }

    getCapitalForValue(carac) {
        let vieinvestie = carac - this.getMinValue();
        if (vieinvestie < 1000) return vieinvestie/4;
        if (vieinvestie < 2002) return (vieinvestie-250)/3;
        return (vieinvestie-834)/2;
    }
}



/*
 *
 *   M P
 *
 */
class MP extends AbstractCaractere {
    constructor(niveau, poireau) {
        super("mp", poireau);
        this.niveau = niveau;
        this.initValues(this.getMinValue(), 0);
    }
    /*
      +1MP : 20 cap
      +2MP : 30 cap
      +3MP : 40 cap
      ...
      +9MP : 100 cap
      +10MP: 100cap
     */
    getMinValue() {
        return 3;
    }

    getMaxValue() {
        return this.getValueForCapital(this.capital + this.niveau.unallocatedCapital);
    }
    
    add_value(nb) {
        let carac = this.value+nb;
        let cap = this.getCapitalForValue(carac);
        while (parseInt(cap) != cap) {
            carac += (nb > 0 ? 1 : -1);
            cap = this.getCapitalForValue(carac);
        }
        this.value = carac;
        this.niveau.allocate_capital(cap - this.capital);
        this.capital = cap;
    }

    get_next_cost() {
        return Math.round(100*(this.getCapitalForValue(this.value+1) - this.capital))/100;
    }
    
    getValueForCapital(cap) {
        let carac = 3;
        let k = 20;
        let som = 0
        while (som+k <= cap) {
            som += k
            k += (k<100 ? 10 : 0);
            carac += 1;
        }
        return carac;
    }

    getCapitalForValue(carac) {
        let investi = carac - 3;
        if (investi < 9) return 10*investi*(investi+1)/2 + 10*investi;
        return 540 + 100*(investi-9);
    }
}


/*
 *
 *   T P
 *
 */
class TP extends AbstractCaractere {
    constructor(niveau, poireau) {
        super("tp", poireau);
        this.niveau = niveau;
        this.initValues(this.getMinValue(), 0);
    }
    /*
      +1TP : 30 cap
      +2TP : 35 cap
      +3TP : 40 cap
      ...
      +9MP : 100 cap
      +10MP: 100 cap
     */
    getMinValue() {
        return 10;
    }

    getMaxValue() {
        return this.getValueForCapital(this.capital + this.niveau.unallocatedCapital);
    }
    
    add_value(nb) {
        let carac = this.value+nb;
        let cap = this.getCapitalForValue(carac);
        while (parseInt(cap) != cap) {
            carac += (nb > 0 ? 1 : -1);
            cap = this.getCapitalForValue(carac);
        }
        this.value = carac;
        this.niveau.allocate_capital(cap - this.capital);
        this.capital = cap;
    }

    get_next_cost() {
        return Math.round(100*(this.getCapitalForValue(this.value+1) - this.capital))/100;
    }
    
    getValueForCapital(cap) {
        let carac = 10;
        let k = 30;
        let som = 0
        while (som+k <= cap) {
            som += k
            k += (k<100 ? 5 : 0);
            carac += 1;
        }
        return carac;
    }

    getCapitalForValue(carac) {
        if (carac < 25) return 5*(carac-10)*(carac+1)/2;
        return 100*carac - 1525;
    }
}



/*
 *
 *   C A R A C T E R E
 *
 */
class Caractere extends AbstractCaractere {
    constructor(identifiant, niveau, poireau) {
        super(identifiant, poireau);
        this.niveau = niveau;
    }
    /*
      carac < 200           1 Capital pour 2 Points de carac
      200 <= carac < 400    1 Capital pour 1 Point de carac
      400 <= carac < 600    2 Capital pour 1 Point de carac
      600 <= carac          3 Capital pour 1 Point de carac      
     */
    getMinValue() {
        return 0;
    }

    getMaxValue() {
        return this.getValueForCapital(this.capital + this.niveau.unallocatedCapital);
    }
    
    add_value(nb) {
        let carac = this.value+nb;
        let cap = this.getCapitalForValue(carac);
        while (parseInt(cap) != cap) {
            carac += (nb > 0 ? 1 : -1);
            cap = this.getCapitalForValue(carac);
        }
        this.value = carac;
        this.niveau.allocate_capital(cap - this.capital);
        this.capital = cap;
    }

    get_next_cost() {
        return Math.round((this.getCapitalForValue(this.value+1) - this.capital)*100)/100;
    }
    
    getValueForCapital(cap) {
        if (cap < 100) return 2*cap;
        if (cap < 300) return 100 + cap;
        if (cap < 700) return parseInt(250 + cap/2);
        return parseInt((cap+1100)/3);
    }

    getCapitalForValue(carac) {
        if (carac < 200) return carac/2;
        if (carac < 400) return carac-100;
        if (carac < 600) return 2*carac-500;
        return 3*carac-1100;
    }
}


/*
 *
 *   A R M E
 *
 */
class Arme {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.level = data.level
        this.min_range = data.min_range;
        this.max_range = data.max_range;
        this.launch_type = data.launch_type;
        this.effects = data.effects;
        this.cost = data.cost;
        this.area = data.area;
        this.los = data.los;
        this.template = data.template; // ???
        this.passive_effects = data.passive_effects;
        this.cooldown = null;
        this.canselect = true;
    }

    get_image() {
        return "<img src=\"https://leekwars.com/image/weapon/" + this.name + ".png\" alt=\"" + this.name + "\" class=\"img-item\">";
    }

    get_name() {
        return this.name + " (" + this.level + ")";
    }
}


/*
 *
 *   P U C E
 *
 */
class Puce {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.level = data.level
        this.min_range = data.min_range;
        this.max_range = data.max_range;
        this.launch_type = data.launch_type;
        this.effects = data.effects;
        this.cost = data.cost;
        this.area = data.area;
        this.los = data.los;
        this.template = data.template; // ???
        this.passive_effects = (data.passive_effects == null ? [] : data.passive_effects);
        this.cooldown = data.cooldown;
        this.initial_cooldown = data.initial_cooldown;
        this.team_cooldown = data.team_cooldown;
        this.canselect = true;
        this.type = data.effects[0].type;
    }

    set_unselectable() {
        this.canselect = false;
    }
    
    get_image() {
        if (!this.canselect) return "";
        return "<img src=\"https://leekwars.com/image/chip/small/" + this.name + ".png\" alt=\"" + this.name + "\" class=\"img-item\">";
    }

    get_name() {
        if (!this.canselect) return this.name;
        return this.name + " (" + this.level + ")";
    }
}





/*
 *
 *   A B S T R A C T   I T E M   L I N E
 *
 */
class AbstractItemLine {
    constructor(type, nb, poireau) {
        if (this.constructor == AbstractItemLine) {
            throw new Error("Abstract class Abstract can't be initialized");
        }
        this.poireau = poireau;
        this.type = type;
        this.nb = nb;
        this.ligne = document.getElementById(type + "-" + nb);
        this.degat = document.getElementById(type + "-" + nb + "-degat");
        this.poison = document.getElementById(type + "-" + nb + "-poison");
        this.entrave = document.getElementById(type + "-" + nb + "-entrave");
        this.boost = document.getElementById(type + "-" + nb + "-boost");
        this.soin = document.getElementById(type + "-" + nb + "-soin");
        this.bouclier = document.getElementById(type + "-" + nb + "-bouclier");
        this.renvoi = document.getElementById(type + "-" + nb + "-renvoi");
        this.autre = document.getElementById(type + "-" + nb + "-autre");
        this.portee = document.getElementById(type + "-" + nb + "-portee");
        this.zone = document.getElementById(type + "-" + nb + "-zone");
        this.cout = document.getElementById(type + "-" + nb + "-cout");
        this.image = document.getElementById(type + "-" + nb + "-image");
        this.cooldown = document.getElementById(type + "-" + nb + "-cooldown");
        this.htmlDepend = [
            this.degat,
            this.poison,
            this.entrave,
            this.boost,
            this.soin,
            this.bouclier,
            this.renvoi,
            this.autre,
            this.portee,
            this.zone,
            this.cout,
            this.image,
        ];
        if (this.cooldown != null) this.htmlDepend.push(this.cooldown);
        this.itemList = [];
        this.is_active = true;
    }

    activate(active) {
        if (!active && this.is_active) {
            this.selectItem(0);
            for (let c=0; c<this.htmlDepend.length; c++) {
                this.htmlDepend[c].innerHTML = "";
            }
        }
        if (active) this.ligne.classList.remove("passive-element");
        else this.ligne.classList.add("passive-element");
        this.is_active = active;
    }

    setListe(liste) {
        this.itemList = liste;
    }

    selectItem(item) {
        throw new Error("Method selectItem must be implemented");        
    }

    getItemValue() {
        throw new Error("Method getItemValue must be implemented");        
    }


    get_niveau_selected() {
        let value = this.getItemValue();
        for (let c=0; c<this.itemList.length; c++) {
            if (value == this.itemList[c].id)
                return this.itemList[c].level;
        }
        return 0;
    }

    maj_item() {
        this.setItem();
        this.poireau.majEverybody();
    }
    
    setItem() {
        let item = null;
        let value = this.getItemValue();
        for (let c=0; c<this.itemList.length; c++) {
            if (value == this.itemList[c].id) {
                item = this.itemList[c];
            }
        }
        if (item == null) {
            for (let c=0; c<this.htmlDepend.length; c++) {
                this.htmlDepend[c].innerHTML = "";
            }
        } else {
            this.update_cooldown(item.cooldown, item.initial_cooldown, item.team_cooldown);
            this.update_zone(item.area, item.los, item.launch_type);
            this.update_degats(item.effects, item.passive_effects);
            this.update_poisons(item.effects, item.passive_effects);
            this.update_soins(item.effects, item.passive_effects);
            this.update_boucliers(item.effects, item.passive_effects);
            this.update_renvois(item.effects, item.passive_effects);
            this.update_autres(item.effects, item.passive_effects);
            this.update_entraves(item.effects, item.passive_effects);
            this.update_boosts(item.effects, item.passive_effects);
            this.cout.innerHTML = item.cost;
            this.portee.innerHTML = item.min_range + (item.min_range == item.max_range ? "" : " - " + item.max_range);
            this.image.innerHTML = item.get_image();
        }
    }

    maj_html_elements() {
        this.setItem();
    }

    update_element_with_tooltip(element, texte, tooltiptext) {
        if (tooltiptext.length > 0) {
            element.classList.add("tooltip");
            element.innerHTML = "<span class=\"tooltiptext\">" + tooltiptext + "</span>" + texte;
        } else {
            element.classList.remove("tooltip");
            element.innerHTML = texte;
        }
    }
    
    update_cooldown(normal, initial, equipe) {
        if (normal != null) {
            let txt = "" + ( normal >= 0 ? normal : "&infin;");
            let msg = "";
            if (initial > 0) {
                txt += " (" + initial + ")";
                msg += "(p): cooldown initial";
            }
            if (equipe > 0) {
                txt += " [" + equipe + "]";
                if (msg.length > 0) msg += "<br />";
                msg += "[p]: cooldown d'équipe";
            }
            this.update_element_with_tooltip(this.cooldown, txt, msg);
        }
    }
    
    update_zone(area, los, launch_type) {
        let txt = "";
        let msg = "";
        switch(area) {
        case 1: break; // point
        case 2: txt = "laser"; msg = "À travers les entités"; break; //laser line
        case 3: txt = "C1"; msg = "En forme de cercle de 3 cases de diamètre"; break; // cercle diametre 3
        case 4: txt = "C2"; msg = "En forme de cercle de 5 cases de diamètre"; break; // cercle diametre 5
        case 5: txt = "C3"; msg = "En forme de cercle de 7 cases de diamètre"; break; // cercle diametre 7
        case 6: txt = "P2"; msg = "En forme de plus de 5 cases de longueur"; break; // plus 2
        case 7: txt = "P3"; msg = "En forme de plus de 7 cases de longueur"; break; // plus 3
        case 8: txt = "X1"; msg = "En forme de croix de 3 cases de longueur"; break; // croix 1
        case 9: txt = "X2"; msg = "En forme de croix de 5 cases de longueur"; break; // croix 2
        case 10: txt = "X3"; msg = "En forme de croix de 7 cases de longueur"; break; // croix 3
        }
        let nolos = (los == 1 ? "": "nolos");
        if (nolos.length * txt.length > 0) { txt += ", " + nolos; msg += ", pas besoin d'être à vue"; }
        else if (nolos.length > 0) { txt = nolos; msg += "pas besoin d'être à vue"; }
        if (launch_type == 0) { // utilisation en ligne
            txt = "en ligne" + (txt.length > 0 ? ",<br />" + txt : "");
        }
        this.update_element_with_tooltip(this.zone, txt, msg);
    }
    
    get_target_string(targets) {
        if (targets == 31) return "";
        let txt = "Affecte ";
        if ((targets & 1) > 0) {
            txt += "les ennemis";
            if (targets > 1) txt += ", ";
        }
        if ((targets & 2) > 0) {
            txt += "les alliers";
            if (targets > 3) txt += ", ";
        }
        if ((targets & 4) > 0) {
            txt += "le lanceur";
            if (targets > 7) txt += ", ";
        }
        if ((targets & 8) > 0) {
            txt += "les poireaux";
            if (targets > 15) txt += ", ";
        }
        if ((targets & 16) > 0) txt += "les bulbes";
        return txt;
    }

    get_modifier_string(modifiers) {
        if (modifiers == 0) return "";
        let txt = "";
        if ((modifiers & 1) > 0) {
            txt += "Accululable";
            if (modifiers > 1) txt += ", ";
        }
        if ((modifiers & 2) > 0) {
            txt += "Multiplié par le nombre de cibles";
            if (modifiers > 3) txt += ", ";
        }
        if ((modifiers & 4) > 0) txt += "sur soi-même";
        return txt;
    }
    
    get_intervalle_string(effect, coeff, nb_suffix, suffix, comment, passif) {
        let msg = "";
        let minval = Math.round(effect.value1 * coeff);
        let maxval = Math.round((effect.value1 + effect.value2) * coeff);
        msg += minval + nb_suffix;
        if (suffix.length > 0) msg += " " + suffix;
        if (minval != maxval) msg += " - " + maxval + nb_suffix;
        if (effect.turns > 0) msg += " (" + effect.turns + "T)";
        if (effect.turns < 0) msg += "%";
        let txt = (passif ? "Effet passif<br />" : "") + comment;
        let mod = this.get_target_string(effect.targets);
        if (txt.length * mod.length > 0) txt += "<br />";
        txt += mod;
        mod = this.get_modifier_string(effect.modifiers);
        if (txt.length * mod.length > 0) txt += "<br />";
        txt += mod;
        let prefix = "";
        if (txt.length > 0) prefix += "<div class=\"tooltip\"><span class=\"tooltiptext\">" + txt + "</span>";
        else prefix = "<div>";
        msg = prefix + msg + "</div>";
        return msg;
    }
    
    get_effects_string(effects, passifs, tab) {
        let txt = "";
        for (let c=0; c<effects.length; c++) {
            let msg = "";
            for (let p=0; p<tab.length; p++) {
                if (effects[c].id == tab[p].id) {
                    msg = this.get_intervalle_string(effects[c], tab[p].coeff, tab[p].nb_suffix, tab[p].suffix, tab[p].comment, false);
                    break;
                }
            }
            txt += msg;
        }
        for (let c=0; c<passifs.length; c++) {
            let msg = "";
            for (let p=0; p<tab.length; p++) {
                if (passifs[c].id == tab[p].id) {
                    msg = this.get_intervalle_string(passifs[c], tab[p].coeff, tab[p].nb_suffix, tab[p].suffix, tab[p].comment, true);
                    break;
                }
            }
            txt += msg;
        }
        if (txt.length > 0) txt = "<div class=\"maxi\">" + txt + "</div>";
        return txt;
    }
    
    update_degats(effects, passifs) {
        let tab = [{"id": 1 , "coeff": 1+this.poireau.force.value/100, "nb_suffix": "", "suffix": "", "comment": ""}, // damage
                   {"id": 25, "coeff": 1+this.poireau.science.value/100, "nb_suffix": "", "suffix": "", "comment": "After Effect"}, // aftereffect
                   {"id": 28, "coeff": 1, "nb_suffix": "%", "suffix": "", "comment": "% vie du lanceur"}, // life damage
                   {"id": 30, "coeff": 1+this.poireau.science.value/100, "nb_suffix": "", "suffix": "", "comment": "Dégâts Nova"}]; // nova damage
        this.degat.innerHTML = this.get_effects_string(effects, passifs, tab);
    }

    update_poisons(effects, passifs) {
        let tab = [{"id": 13, "coeff": 1+this.poireau.magie.value/100, "nb_suffix": "", "suffix": "", "comment": ""}]; // poison
        this.poison.innerHTML = this.get_effects_string(effects, passifs, tab);
    }

    update_soins(effects, passifs) {
        let tab = [{"id": 2 , "coeff": 1+this.poireau.sagesse.value/100, "nb_suffix": "", "suffix": "", "comment": ""}, // heal
                   {"id": 12, "coeff": 1+this.poireau.sagesse.value/100, "nb_suffix": "", "suffix": "", "comment": "Vie Max"}]; // boost max life
        this.soin.innerHTML = this.get_effects_string(effects, passifs, tab);
    }

    update_boucliers(effects, passifs) {
        let tab = [{"id": 5, "coeff": 1+this.poireau.resistance.value/100, "nb_suffix": "%", "suffix": "", "comment": ""}, // relative shield
                   {"id": 6, "coeff": 1+this.poireau.resistance.value/100, "nb_suffix": "", "suffix": "", "comment": ""}]; // absolute shield
        // ?? steal absolute shield
        this.bouclier.innerHTML = this.get_effects_string(effects, passifs, tab);
    }

    update_renvois(effects, passifs) {
        let tab = [{"id": 20, "coeff": 1+this.poireau.agilite.value/100, "nb_suffix": "%", "suffix": "", "comment": ""}]; // damage return
        this.renvoi.innerHTML =this.get_effects_string(effects, passifs, tab);
    }

    update_autres(effects, passifs) {
        let txt = "";
        for (let c=0; c<effects.length; c++) {
            let msg = "";
            if (effects[c].id == 9) { // debuff
                msg += "-60% effets";
            } else if (effects[c].id == 10) { // teleport
                msg += "teleport";
            } else if (effects[c].id == 11) { // invert
                msg += "invertion";
            } else if (effects[c].id == 14) { // summon
                msg += "summon";
            } else if (effects[c].id == 15) { // resurect
                msg += "resurection";
            } else if (effects[c].id == 16) { // kill
                msg += "kill";
            } else if (effects[c].id == 23) { // antidote
                msg += "anti-poison";
            }
            if (msg.length > 0) {
                if (txt.length > 0) txt += "<br />";
                txt += msg;
            }
        }
        this.autre.innerHTML = txt;
    }

    update_entraves(effects, passifs) {
        let tab = [{"id": 17, "coeff": 1+this.poireau.magie.value/100, "nb_suffix": "", "suffix": "MP", "comment": ""}, // shackle mp
                   {"id": 18, "coeff": 1+this.poireau.magie.value/100, "nb_suffix": "", "suffix": "TP", "comment": ""}, // shackle tp
                   {"id": 19, "coeff": 1+this.poireau.magie.value/100, "nb_suffix": "", "suffix": "Force", "comment": ""}, // shackle strength
                   {"id": 24, "coeff": 1+this.poireau.magie.value/100, "nb_suffix": "", "suffix": "Magie", "comment": ""}, // shackle magic
                   {"id": 26, "coeff": 1, "nb_suffix": "%", "suffix": "", "comment": "Vulnérabilité"}, // vulnerability
                   {"id": 27, "coeff": 1, "nb_suffix": "", "suffix": "", "comment": "Vulnérabilité"}]; // absolute vulnerability
        this.entrave.innerHTML = this.get_effects_string(effects, passifs, tab);
    }

    
    update_boosts(effects, passifs) {
        let tab = [{"id": 3 , "coeff": 1+this.poireau.science.value/100, "nb_suffix": "", "suffix": "Force", "comment": ""}, // buff strenght
                   {"id": 4 , "coeff": 1+this.poireau.science.value/100, "nb_suffix": "", "suffix": "Agi", "comment": ""}, // buff agility
                   {"id": 7 , "coeff": 1+this.poireau.science.value/100, "nb_suffix": "", "suffix": "MP", "comment": ""}, // buff mp
                   {"id": 8 , "coeff": 1+this.poireau.science.value/100, "nb_suffix": "", "suffix": "TP", "comment": ""}, // buff tp
                   {"id": 21, "coeff": 1+this.poireau.science.value/100, "nb_suffix": "", "suffix": "Rési", "comment": ""}, // buff resistance
                   {"id": 22, "coeff": 1+this.poireau.science.value/100, "nb_suffix": "", "suffix": "Sag", "comment": ""}, // buff wisdom
                   {"id": 31, "coeff": 1, "nb_suffix": "", "suffix": "MP", "comment": ""}, // raw buff mp
                   {"id": 32, "coeff": 1, "nb_suffix": "", "suffix": "TP", "comment": ""}, // raw buff tp
                   {"id": 33, "coeff": 1, "nb_suffix": "", "suffix": "", "comment": "Poison reçu converti en Science"}, // poison to science
                   {"id": 34, "coeff": 1, "nb_suffix": "", "suffix": "", "comment": "Dégâts reçus convertis en Bouclier Absolu"}, // damage to absolute shield
                   {"id": 35, "coeff": 1, "nb_suffix": "", "suffix": "", "comment": "Dégâts reçus convertis en Force"}, // damage to strength
                   {"id": 36, "coeff": 1, "nb_suffix": "", "suffix": "", "comment": "Dégâts Nova reçus convertis en Magie"}, // nova damage to magic
                   {"id": 37, "coeff": 1, "nb_suffix": "", "suffix": "Boucl. Abs", "comment": ""}, // raw absolute shield
                   {"id": 38, "coeff": 1, "nb_suffix": "", "suffix": "Force", "comment": ""}, // raw buff strength
                   {"id": 39, "coeff": 1, "nb_suffix": "", "suffix": "Magie", "comment": ""}, // raw buff magic
                   {"id": 40, "coeff": 1, "nb_suffix": "", "suffix": "Science", "comment": ""}, // raw buff science
                   {"id": 41, "coeff": 1, "nb_suffix": "", "suffix": "Agilité", "comment": ""}]; // raw buff agility
        this.boost.innerHTML = this.get_effects_string(effects, passifs, tab);
    }
    
}


/*
 *
 *   I T E M   L I N E
 *
 */
class ItemLine extends AbstractItemLine {
    constructor(type, nb, poireau) {
        super(type, nb, poireau);
        this.liste = document.getElementById(type + "-" + nb + "-nom");
        let myself = this;
        this.liste.addEventListener("change", function() { myself.maj_item(); });
    }

    activate(active) {
        super.activate(active);
        if (!active) this.liste.value = "0";
    }

    setListe(liste) {
        super.setListe(liste);
        let txt = "";
        for (let c=0; c<liste.length; c++) {
            txt += "<option id=\"" + this.type + "-" + this.nb + "-" + liste[c].id + "\" value=\"" + liste[c].id + "\"";
            if (!liste[c].canselect) txt += " disabled";
            txt += ">" + liste[c].get_name() + "</option>";
        }
        this.liste.innerHTML += txt;
        this.maj_html_elements();
    }

    selectItem(item) {
        this.liste.value = item;
    }

    getItemValue() {
        return this.liste.value;
    }
    
    maj_html_elements() {
        if (this.itemList != null) {
            for (let c=0; c<this.itemList.length; c++) {
                let selectable = (this.itemList[c].level > 0) && (this.poireau.niveau.value >= this.itemList[c].level);
                document.getElementById(this.type + "-" + this.nb + "-" + this.itemList[c].id).disabled = !selectable;
            }
        }
        super.maj_html_elements();
    }

    desactivate_item(id) {
        if (this.itemList != null) {
            document.getElementById(this.type + "-" + this.nb + "-" + id).disabled = true;
        }
    }
}



/*
 *
 *   I T E M S   E Q U I P E S
 *
 */
class ItemsEquipes {
    constructor(niveau, poireau, nb_items, type) {
        this.niveau = niveau;
        this.items = [];
        this.poireau = poireau;
        this.nb_items = nb_items;
        let nbmax = nb_items[nb_items.length-1][1];
        this.nb_items.push([302,nbmax]);
        for (let c=0; c<nbmax; c++) {
            this.items.push(new ItemLine(type, c, poireau));
        }
        poireau.addMAJable(this);
    }

    set(liste) {
        for(let c=0; c<this.items.length; c++) {
            this.items[c].setListe(liste);
        }
    }

    selectItems(tab) {
        for (let c=0; c<tab.length; c++) { this.items[c].selectItem(tab[c]); }
        for (let c=tab.length; c<this.items.length; c++) { this.items[c].selectItem("0"); }
    }

    sort_me(sort_fct) {
        let selectitems = [];
        let itemlist = this.items[0].itemList;
        for (let c=0; c<this.items.length; c++) {
            let itemid = this.items[c].getItemValue();
            if (itemid < 1) continue;
            let k=0;
            while (itemid != itemlist[k].id) k++;
            selectitems.push(itemlist[k]);
        }
        selectitems.sort(sort_fct);
        for (let c=0; c<selectitems.length; c++) selectitems[c] = selectitems[c].id;
        this.selectItems(selectitems);
    }
    
    get_nb_max_item() {
        let k = 0;
        while (this.niveau.value >= this.nb_items[k][0]) { k++; }
        return this.nb_items[k-1][1];
    }
    
    maj_html_elements() {
        let nb = this.get_nb_max_item();
        for (let c=0; c<nb; c++) {
            this.items[c].activate(true);
            this.items[c].maj_html_elements();
            for (let k=0; k<nb; k++) {
                if (k == c || this.items[k].getItemValue() == 0) continue;
                this.items[c].desactivate_item(this.items[k].getItemValue());
            }
        }
        for (let c=nb; c<this.items.length; c++) {
            this.items[c].activate(false);
        }
    }

    get_niveau_required() {
        let lvl = 0;
        for (let c=0; c<this.items.length; c++) {
            let lvlitem = this.items[c].get_niveau_selected();
            lvl = (lvl < lvlitem ? lvlitem : lvl);
        }
        return lvl;
    }
}




/*
 *
 *   S I M U L   I T E M   L I N E
 *
 */
class SimulItemLine extends AbstractItemLine {
    constructor(type, nb, poireau, equipement) {
        super(type, nb, poireau);
        this.nom = document.getElementById(type + "-" + nb + "-nom");
        this.equipe = document.getElementById(type + "-" + nb + "-equipe");
        let myself = this;
        this.equipe.addEventListener("click", function() { myself.click_equipement(); });
        this.item = null;
        this.equipement = equipement;
    }

    selectItem(item) {
        this.item = item;
        this.nom.innerHTML = item.name;
    }

    getItemValue() {
        return this.item.id;
    }

    get_niveau_selected() {
        return this.item.level;
    }

    maj_html_elements() {
        super.maj_html_elements();
        let equiped = false;
        let nbselect = 0;
        for (let c=0; c<this.equipement.items.length; c++) {
            if (this.equipement.items[c].getItemValue() != 0) nbselect++;
            if (this.equipement.items[c].getItemValue() == this.item.id) equiped = true;
        }
        this.equipe.checked = equiped;
        if (this.equipe.checked) {
            this.equipe.disabled = false;
        } else {
            this.equipe.disabled = (this.equipement.get_nb_max_item() == nbselect);
        }
    }

    click_equipement() {
        if (this.equipe.checked) {
            for (let c=0; c<this.equipement.items.length; c++) {
                if (this.equipement.items[c].getItemValue() == 0) {
                    this.equipement.items[c].selectItem(this.item.id);
                    break;
                }
            }
        } else {
            for (let c=0; c<this.equipement.items.length; c++) {
                if (this.equipement.items[c].getItemValue() == this.item.id) {
                    this.equipement.items[c].selectItem(0);
                    break;
                }
            }
        }
        this.poireau.majEverybody();
    }
}


/*
 *
 *   S I M U L A T I O N   A R M E S
 *
 */
class SimulationArmes {
    constructor(poireau) {
        this.poireau = poireau;
        this.parent = document.getElementById("simu-armetable");
        this.ListeArmes = []; // liste des armes
        this.childArmes = []; // liste des elements html
        this.listeLines = []; // liste des ItemLine
        poireau.addMAJable(this);
    }

    set(liste) {
        let caracs = ["degat", "poison", "entrave", "boost", "soin", "bouclier", "renvoi", "autre", "portee", "zone", "cout"];
        this.ListeArmes = liste;
        for (let k=0; k<liste.length; k++) {
            let element = document.createElement("TR");
            element.id = "simu-arme-" + k;
            let txt = "<td><input type=\"checkbox\" id=\"simu-arme-" + k + "-equipe\"></td>";
            txt += "<td id=\"simu-arme-" + k + "-nom\"></td>";
            for (let c=0; c<caracs.length; c++) {
                txt += "<td id=\"simu-arme-" + k + "-" + caracs[c] + "\" class=\"tab-" + caracs[c] + "\"></td>";
            }
            txt += "<td id=\"simu-arme-" + k + "-image\" class=\"parent-img-item\"></td>";
            element.innerHTML = txt;
            this.childArmes.push(element);
            this.parent.append(element);
            let sil = new SimulItemLine("simu-arme", k, this.poireau, this.poireau.armes);
            sil.setListe(liste);
            this.listeLines.push(sil);
            sil.selectItem(liste[k]);
        }
    }

    maj_html_elements() {
        for (let k=0; k<this.ListeArmes.length; k++) {
            if (this.ListeArmes[k].level > this.poireau.niveau.value) {
                this.childArmes[k].classList.add("passive-element");
            } else {
                this.childArmes[k].classList.remove("passive-element");
            }
            this.listeLines[k].maj_html_elements();
        }
    }
}


/*
 *
 *   S I M U L A T I O N   P U C E S
 *
 */
class SimulationPuces {
    constructor(poireau) {
        this.poireau = poireau;
        this.selected_type = 0;
        this.parent = document.getElementById("simu-pucetable");
        this.ListePuces = []; // liste des puces
        this.childPuces = []; // liste des elements html
        this.listeLines = []; // liste des ItemLine
        poireau.addMAJable(this);
    }

    set(liste) {
        this.ListePuces = liste;
        let caracs = ["degat", "poison", "entrave", "boost", "soin", "bouclier", "renvoi", "autre", "portee", "zone", "cout", "cooldown"];
        for (let k=0; k<liste.length; k++) {
            if (liste[k].level == 0) continue;
            let element = document.createElement("TR");
            element.id = "simu-puce-" + k;
            let txt = "<td><input type=\"checkbox\" id=\"simu-puce-" + k + "-equipe\"></td>";
            txt += "<td id=\"simu-puce-" + k + "-nom\"></td>";
            for (let c=0; c<caracs.length; c++) {
                txt += "<td id=\"simu-puce-" + k + "-" + caracs[c] + "\" class=\"tab-" + caracs[c] + "\"></td>";
            }
            txt += "<td id=\"simu-puce-" + k + "-image\" class=\"parent-img-item\"></td>";
            element.innerHTML = txt;
            this.childPuces.push(element);
            this.parent.append(element);
            let sil = new SimulItemLine("simu-puce", k, this.poireau, this.poireau.puces);
            sil.setListe(liste);
            this.listeLines.push(sil);
            sil.selectItem(liste[k]);
        }
        this.set_selected(1);
    }

    set_selected(sel) {
        this.selected_type = sel;
        for (let k=1; k<10; k++) {
            document.getElementById("bouton_puces_type_" + k).disabled = (k == sel);
        }
    }
    
    maj_html_elements() {
        for (let k=0; k<this.listeLines.length; k++) {
            let v = this.listeLines[k].getItemValue();
            if (this.listeLines[k].get_niveau_selected() > this.poireau.niveau.value || this.selected_type != this.listeLines[k].item.type) {
                this.childPuces[k].classList.add("passive-element");
            } else {
                this.childPuces[k].classList.remove("passive-element");
            }
            this.listeLines[k].maj_html_elements();
        }
    }
}












var leek = null;

/*
 * Initialisation des Caracteres
 */
function initRestator() {
    initRestatorPage();
    leek = new Poireau();
    $.post("https://leekwars.com/api/weapon/get-all", "", initArmeSync);
    $.post("https://leekwars.com/api/chip/get-all", "", initPuceSync);
    switch_simu("poireau");
}

var __init = 0;
function importFromURL() {
    __init++;
    if (__init < 2) return;
    let ind = window.location.href.indexOf("?");
    if (ind != -1) {
        let sauve = window.location.href.substring(ind+1);
        importLeek(sauve);
    }
}

function importFromPage() {
    importLeek(document.getElementById("id-leek").value);
}

function importLeek(idleek) {
    let digitReg = new RegExp("^[0-9]+$");
    let alphaReg = new RegExp("^[a-zA-Z]+$");
    if (digitReg.test(idleek)) {
        console.log("import leek id: " + idleek);
        $.get("https://leekwars.com/api/leek/get/" + idleek, "", syncImportLeek).fail(failImportLeek);
    } else if (alphaReg.test(idleek)) {
        console.log("import leek save: " + idleek);
        let data = leek.decode(idleek);
        if (data !== null) leek.reset_leek(data);
        else document.getElementById("leek-name").innerHTML = "sauvegarde non valide";
    } else document.getElementById("leek-name").innerHTML = "identifiant ou sauvegarde non valide";
}

function failImportLeek(data) {
    document.getElementById("leek-name").innerHTML = "poireau non trouvé";
}

function syncImportLeek(data) {
    leek.reset_leek(data);
    document.getElementById("leek-name").innerHTML = data.name;
}

function initArmeSync(data) {
    let armes = [];
    for (let c in data.weapons) {
        armes.push(new Arme(data.weapons[c]));
    }
    armes.sort(trie_par_niveau);
    leek.initArmes(armes);
    importFromURL();
}

function initPuceSync(data) {
    let puces = [];
    for (let c in data.chips) {
        puces.push(new Puce(data.chips[c]));
    }
    let factices = [{"name": "#### DEGATS ####", "level": 0, "id": -1, "effects":[{"type": 1}]},
                    {"name": "#### SOINS ####", "level": 0, "id": -2, "effects":[{"type": 2}]},
                    {"name": "#### BOOSTS ####", "level": 0, "id": -3, "effects":[{"type": 3}]},
                    {"name": "#### BOUCLIERS ####", "level": 0, "id": -4, "effects":[{"type": 4}]},
                    {"name": "#### TACTIQUE ####", "level": 0, "id": -5, "effects":[{"type": 5}]},
                    {"name": "#### RENVOIS ####", "level": 0, "id": -6, "effects":[{"type": 6}]},
                    {"name": "#### POISONS ####", "level": 0, "id": -7, "effects":[{"type": 7}]},
                    {"name": "#### BULBES ####", "level": 0, "id": -8, "effects":[{"type": 8}]},
                    {"name": "#### ENTRAVES ####", "level": 0, "id": -9, "effects":[{"type": 9}]}];
    for (let k=0; k<factices.length; k++) {
        let pupuce = new Puce(factices[k]);
        pupuce.set_unselectable();
        puces.push(pupuce);
    }
    puces.sort(trie_par_type);
    leek.initPuces(puces);
    importFromURL();
}

function initRestatorPage() {
    let caracs = ["vie", "force", "sagesse", "agilite", "resistance", "science", "magie", "frequence", "mp", "tp"];
    let noms = ["Vie", "Force", "Sagesse", "Agilité", "Résistance", "Science", "Magie", "Fréquence", "Point Mouvement", "Point Tour"];
    let buttid = ["-plus-un", "-moins-un", "-plus-dix", "-moins-dix", "-plus-cent", "-moins-cent"];
    let buttval = ["+1", "-1", "+10", "-10", "+100", "-100"];
    let txt = "";
    for (let c=0; c<noms.length; c++) {
        txt += "<tr><th class=\"a-droite\"><span id=\"" + caracs[c] + "-label\" class=\"labels\"> " + noms[c] + " : <span id=\"" + caracs[c] + "-nb\">0</span></th>";
        for (let k=0; k<buttid.length; k++)
            txt += "<td><button id=\"" + caracs[c] + buttid[k] + "\" class=\"" + caracs[c] + " boutons\">" + buttval[k] + "</button></td>";
        txt += "<td class=\"tooltip\"><span class=\"tooltiptext\">(Capital investi)<br />[coût du point suivant]</span>(<span id=\"" + caracs[c] + "-capital\">0</span>) [<span id=\"" + caracs[c] + "-next\">0</span>]</td></tr>";
    }
    document.getElementById("caracteres").innerHTML += txt;
    
    txt = "<tr>";
    caracs = ["degat", "poison", "entrave", "boost", "soin", "bouclier", "renvoi", "autre", "portee", "zone", "cout"];
    noms = ["Nom", "Dégats", "Poison", "Entrave", "Boost", "Soin", "Bouclier", "Renvoi", "Autre", "Portée", "Zone", "Coût", "Image"];
    for (let c=0; c<noms.length; c++)
        txt += "<th>" + noms[c] + "</th>";
    txt += "</tr>";
    for (let c=0; c<4; c++) {
        txt += "<tr id=\"arme-" + c + "\"><td><select id=\"arme-" + c + "-nom\"> <option value=\"0\">---</option></select></td>";
        for (let k=0; k<caracs.length; k++) {
            txt += "<td id=\"arme-" + c + "-" + caracs[k] + "\" class=\"tab-" + caracs[k] + "\"></td>";
        }
        txt += "<td id=\"arme-" + c + "-image\" class=\"parent-img-item\"></td></tr>";
    }
    document.getElementById("armetable").innerHTML += txt;
    
    txt = "<tr>";
    caracs = ["degat", "poison", "entrave", "boost", "soin", "bouclier", "renvoi", "autre", "portee", "zone", "cout", "cooldown"];
    noms = ["Nom", "Dégats", "Poison", "Entrave", "Boost", "Soin", "Bouclier", "Renvoi", "Autre", "Portée", "Zone", "Coût", "Cooldown", "Image"];
        for (let c=0; c<noms.length; c++)
        txt += "<th>" + noms[c] + "</th>";
    txt += "</tr>";
    for (let c=0; c<20; c++) {
        txt += "<tr id=\"puce-" + c + "\"><td><select id=\"puce-" + c + "-nom\"><option value=\"0\">---</option></select></td>";
        for (let k=0; k<caracs.length; k++) {
            txt += "<td id=\"puce-" + c + "-" + caracs[k] + "\" class=\"tab-" + caracs[k] + "\"></td>";
        }
        txt += "<td id=\"puce-" + c + "-image\" class=\"parent-img-item\"></td></tr>";
    }
    document.getElementById("pucetable").innerHTML += txt;
}

function trie_par_type(a,b) {
    if (a.type != b.type)
        return a.type - b.type;
    return a.level - b.level;
}

function trie_par_niveau(a,b) {
    return a.level - b.level;
}



function switch_puce_type(k) {
    leek.simulPuce.set_selected(k);
    leek.simulPuce.maj_html_elements();
}


function switch_simu(texte) {
    if (texte == "poireau") {
        document.getElementById("simu-poireau").classList.remove("passive-element");
        document.getElementById("simu-armes").classList.add("passive-element");
        document.getElementById("simu-puces").classList.add("passive-element");
    } else if (texte == "armes") {
        document.getElementById("simu-poireau").classList.add("passive-element");
        document.getElementById("simu-armes").classList.remove("passive-element");
        document.getElementById("simu-puces").classList.add("passive-element");
    } else if (texte == "puces") {
        document.getElementById("simu-poireau").classList.add("passive-element");
        document.getElementById("simu-armes").classList.add("passive-element");
        document.getElementById("simu-puces").classList.remove("passive-element");
    }
    document.getElementById("bouton-poireau").disabled = (texte == "poireau");
    document.getElementById("bouton-armes").disabled = (texte == "armes");
    document.getElementById("bouton-puces").disabled = (texte == "puces");
    leek.majEverybody();
}

function item_sort(sort_type) {
    if (sort_type == "type") {
        leek.puces.sort_me(trie_par_type);
        leek.armes.sort_me(trie_par_type);
    }
    if (sort_type == "niveau") {
        leek.puces.sort_me(trie_par_niveau);
        leek.armes.sort_me(trie_par_niveau);
    }
    leek.majEverybody();
}


function hide_show_import_export() {
    document.getElementById("import-export").classList.toggle("passive-element");
}

/*
 * Fin script
 */
